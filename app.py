#!/usr/bin/python

import liquidcrystal_i2c
from random import randint
import subprocess
import sqlite3
from datetime import datetime
from datetime import timedelta
import time
import RPi.GPIO as GPIO  

print("starting application")

todaysSupport = "Ingen "
lastSaySupport = datetime.now() - timedelta(days=10)


funfacts = ["Banging your head against a wall for one hour burns 150 calories.",
            "I am darth snowman from the viking village and I will rule the galaxy.",
            "Looking good today.",
            "I wont tell you."
            "Kebab for lunch."]

cols = 16
rows = 2

lcd = liquidcrystal_i2c.LiquidCrystal_I2C(0x27, 1, numlines=rows)

def setSupport(channel):
    
    #Fix for ignoring double calls
    if GPIO.input(channel) == 0:
        return
    
    support = []
    position = []
    
    # Gets support from DB
    sql = "select * from tomtePeople where hasbeen = 0"
    
    conn = sqlite3.connect('/home/pi/hackaton/tomteLista.db')
    
    cur = conn.cursor()
    cur.execute(sql)
    
    rows = cur.fetchall()
    
    for row in rows:
        print(row[1])
        support.append(row[1])
        position.append(row[2])
    
    #clear supportflag if everyone has hasbeen == true
    if len(support) == 0:
        conn.execute("update tomtePeople set hasbeen = 0")
        cur.execute(sql)
        rows = cur.fetchall()
        
        for row in rows:
            print(row[1])
            support.append(row[1])
    
    # rand based on supportlist from db
    supportnum = randint(0,len(support)-1)
    print(supportnum)
    global todaysSupport
    todaysSupport = support[supportnum]
    
    cur.execute("update tomtePeople set lastbeen = 0")
    cur.execute("update tomtePeople set hasbeen = 1, lastbeen = 1 where name = '"+todaysSupport+"'")
    conn.commit()
    
    print("testing lcd")
    print(todaysSupport + " har support")

    global pwm
    print("starting spin")
    pwm.start(1)
    print("sleeping")
    time.sleep(3)
    print("changing cycle to 8")
    pwm.ChangeDutyCycle(8)
    print("sleeping")
    time.sleep(3)
    print("stopping")
#    pwm.stop()
    
    global lcd
    lcd.printline(1, support[supportnum].center(cols))
    saySupport(support[supportnum], True)
    
def getLastSupport():
    # Gets last support from DB
    print('started getSupport')
    sql = "select name from tomtePeople where lastbeen = 1"
    conn = sqlite3.connect('/home/pi/hackaton/tomteLista.db')
    cur = conn.cursor()
    cur.execute(sql)
    print('Finished getting')
    
    rows = cur.fetchall()
    print(rows[0])
    print(len(rows))
    print(rows[0][0])
    return rows[0][0];
    
def sayTodaysSupport(channel):
    if GPIO.input(channel) == 0:
        return
    
    global todaysSupport
    saySupport(todaysSupport)

def say(input):
    subprocess.call(["espeak","-s 100", input])

def saySupport(support, override = False):
    global lastSaySupport
    
    #Say something radom occasionally
    randomSay = randint(0,4)
    if randomSay == 0:
        say(funfacts[randint(0, len(funfacts)-1)])
    else:
        if override or lastSaySupport < datetime.now() - timedelta(seconds=5):
            lastSaySupport = datetime.now()
            say(support + " har support")
            
#GPIO.cleanup()

GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)

#Button pin
GPIO.setup(10, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(7, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(35, GPIO.OUT)

pwm=GPIO.PWM(35,50)

def setDutyCycle(dutyCycle, sleeptime = 2):
    print("Dutycycle is: " +str(dutyCycle))
    global pwm
    pwm.ChangeDutyCycle(dutyCycle)
    time.sleep(sleeptime)
    

pwm.start(8)

#Add callback events
GPIO.add_event_detect(10, GPIO.RISING,callback = setSupport)
GPIO.add_event_detect(7, GPIO.RISING,callback = sayTodaysSupport)

lcd = liquidcrystal_i2c.LiquidCrystal_I2C(0x27, 1, numlines=rows)
    
lcd.printline(0, 'Ragnarz Support'.center(cols))
lastSupport = getLastSupport()
lcd.printline(1, lastSupport.center(cols))
while True:
    time.sleep(1000)
#message  = input("derp\n")
