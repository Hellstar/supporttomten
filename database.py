import sqlite3



def create_table(conn, create_table_sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)




def create_member(conn, newMember):
 
    sql = ''' INSERT INTO tomtePeople(id, name, hasbeen, lastbeen)
              VALUES(?,?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql, newMember)
    return cur.lastrowid




def select_all_members(conn):
    cur = conn.cursor()
    cur.execute("SELECT * FROM tomtePeople")
 
    rows = cur.fetchall()
 
    for row in rows:
        print(row)


def drop_table(conn):
    cur = conn.cursor()
    cur.execute("DROP TABLE tomtePeople")


conn = sqlite3.connect('tomteLista.db')
drop_table(conn);


 
sql_create_projects_table = """ CREATE TABLE IF NOT EXISTS tomtePeople (
                                    id integer PRIMARY KEY,
                                    name text NOT NULL,
                                    hasbeen bool NOT NULL,
                                    lastbeen bool NOT NULL
                                    ); """
                                
                                
create_table(conn, sql_create_projects_table)

newMember_1 = (1, 'Steffan', 0, 0)
newMember_2 = (2, 'Rezo', 0, 0)
newMember_3 = (3, 'Sofie', 0, 0)
newMember_4 = (4, 'Daniel', 0, 0)
newMember_5 = (5, 'Simon', 0, 0)
 
create_member(conn, newMember_1)
create_member(conn, newMember_2)
create_member(conn, newMember_3)
create_member(conn, newMember_4)
create_member(conn, newMember_5)

select_all_members(conn)
conn.commit()
